package khmer.org.model;

import javax.json.bind.annotation.JsonbProperty;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel
public class Person {
	@JsonbProperty("ID")
	@ApiModelProperty(value = "ID", example = "")
	private Integer id;

	@JsonbProperty("NAME")
	@ApiModelProperty(value = "NAME", example = "")
	private String name;

	@JsonbProperty("GENDER")
	@ApiModelProperty(value = "GENDER", example = "")
	private String gender;

	@JsonbProperty("TEL")
	@ApiModelProperty(value = "TEL", example = "")
	private String tel;

	public Person() {
		// TODO Auto-generated constructor stub
	}

	public Person(Integer id, String name, String gender, String tel) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.tel = tel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
}
