package khmer.org.restController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import khmer.org.customException.DataNotFoundException;
import khmer.org.model.UploadDetails;

@Path("/file")
@Api("/file")
@SwaggerDefinition(tags = { @Tag(name = "File upload", description = "Upload Services") })
public class UploadController {

	private static final String UPLOAD_PATH = "C:/uploadedFiles/";
	private Map<String, Object> rs = null;
	private String randomeFileName = "";
	private UploadDetails uploadDetails = null;
	private List<UploadDetails> allUploadFiles = null;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response index() {
		rs = new HashMap<String, Object>();
		rs.put("URL", "");
		rs.put("SIZE", "");
		rs.put("ORIGINAL_NM", "");
		return Response.status(Response.Status.OK).entity(rs).build();
	}

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Single file upload")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "file", value = "choose file", required = true, dataType = "java.io.File", paramType = "form") })
	public Response singleUploadFile(@ApiParam(hidden = true) @FormDataParam("file") InputStream fileInputStream,
			@ApiParam(hidden = true) @FormDataParam("file") FormDataContentDisposition fileDetails) {
		System.out.println("_data : " + fileDetails);
		File theDir = new File(UPLOAD_PATH);
		if (!theDir.exists()) {
			theDir.mkdir();
		}
		String fileName = fileDetails.getFileName();
		String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
		randomeFileName = UUID.randomUUID().toString();
		fileName = randomeFileName + "." + extension;
		saveFile(fileInputStream, fileName);
		rs = new HashMap<String, Object>();
		File file = new File(UPLOAD_PATH + fileName);
		Long fileSize = file.length(); // in bytes
		uploadDetails = new UploadDetails(fileName, "/image/" + fileName, fileSize, UPLOAD_PATH);

		rs.put("RESPONSE", uploadDetails);
		rs.put("CODE", "0000");
		rs.put("MESSAGE", "OK");
		/*
		 * rs.put("URL", "/image/" + fileName); rs.put("SERVER_URL", UPLOAD_PATH);
		 * rs.put("SIZE", fileDetails.getSize()); rs.put("ORIGINAL_NM",
		 * fileDetails.getFileName());
		 */
		return Response.status(Response.Status.OK).entity(rs).build();
	}

	@POST
	@Path("/uploads")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Multiple files upload")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "files", value = "choose file", required = true, dataType = "java.io.File", paramType = "form", allowableValues = "range[0, 100]") })
	public Response multiUploadFile(@ApiParam(hidden = true) @FormDataParam("files") List<FormDataBodyPart> bodyParts,
			@ApiParam(hidden = true) @FormDataParam("files") FormDataContentDisposition fileDetails) {

		allUploadFiles = new ArrayList<UploadDetails>();
		StringBuffer fileInfo = new StringBuffer("");
		/* Save multiple files */
		for (int i = 0; i < bodyParts.size(); i++) {
			/*
			 * Casting FormDataBodyPart to BodyPartEntity, which can give us InputStream for
			 * uploaded file
			 */
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
			String fileName = bodyParts.get(i).getContentDisposition().getFileName();
			String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
			randomeFileName = UUID.randomUUID().toString();
			fileName = randomeFileName + "." + extension;

			saveFile(bodyPartEntity.getInputStream(), fileName);
			File file = new File(UPLOAD_PATH + fileName);
			Long fileSize = file.length(); // in bytes
			uploadDetails = new UploadDetails(fileName, "/image/" + fileName, fileSize, UPLOAD_PATH);
			allUploadFiles.add(uploadDetails);
			fileInfo.append(UPLOAD_PATH + fileName);
		}

		rs = new HashMap<String, Object>();
		rs.put("RESPOSNE", allUploadFiles);
		rs.put("CODE", "0000");
		rs.put("MESSAGE", "OK");
		return Response.status(Response.Status.OK).entity(rs).build();
	}

	private void saveFile(InputStream file, String fileName) {
		try {
			/* Change directory path */
			java.nio.file.Path path = Paths.get(UPLOAD_PATH, fileName);
			/* Save InputStream as file */
			Files.copy(file, path);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

}
