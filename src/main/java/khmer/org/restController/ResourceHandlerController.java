package khmer.org.restController;

import java.io.File;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

@Path("/image")
@Api("/image")
@SwaggerDefinition(tags = { @Tag(name = "Image Source", description = "Configuration Services") })
public class ResourceHandlerController {

	@GET
	@Path("{image}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "View Image locate folder")
	public Response getImage(@PathParam("image") String image) {
		File f = new File("C:\\uploadedFiles\\" + image);
		if (!f.exists()) {
			throw new WebApplicationException(404);
		}
		String mt = new MimetypesFileTypeMap().getContentType(f);
		return Response.ok(f, mt).build();
	}

}
