package khmer.org.config;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
public class RequestFilterConfig implements ContainerRequestFilter {

	public void filter(ContainerRequestContext requestContext) throws IOException {
		System.out.println("Print requested!");
		String methodOverride = requestContext.getHeaderString("X-Http-Method-Override");
		if (methodOverride != null)
			requestContext.setMethod(methodOverride);
	}

}
